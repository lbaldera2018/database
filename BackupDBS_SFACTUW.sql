USE [DBSFactuW]
GO
/****** Object:  Table [dbo].[ArticulosVendidos]    Script Date: 2/20/2020 10:05:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ArticulosVendidos](
	[IdArticuloVendido] [int] IDENTITY(1,1) NOT NULL,
	[IdVenta] [int] NOT NULL,
	[IdArticulo] [int] NOT NULL,
	[Codigo] [varchar](10) NOT NULL,
	[Cantidad] [int] NOT NULL,
	[PrecioUnitario] [decimal](18, 2) NOT NULL,
	[MontoTotal] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_ArticulosVendidos] PRIMARY KEY CLUSTERED 
(
	[IdArticuloVendido] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TbArticulos]    Script Date: 2/20/2020 10:05:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TbArticulos](
	[IdArticulo] [int] IDENTITY(1,1) NOT NULL,
	[Codigo] [varchar](10) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[IdCategoria] [int] NULL,
	[PrecioUnitario] [decimal](18, 2) NOT NULL,
	[EliminadoPor] [varchar](50) NULL,
	[EliminadoEn] [datetime] NULL,
	[PrecioAlPorMayor] [decimal](18, 2) NULL,
 CONSTRAINT [PK_TbArticulos] PRIMARY KEY CLUSTERED 
(
	[IdArticulo] ASC,
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TbCategoria]    Script Date: 2/20/2020 10:05:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TbCategoria](
	[IdCategoria] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[EliminadoPor] [varchar](50) NULL,
	[EliminadoEn] [datetime] NULL,
 CONSTRAINT [PK_TbCategoria] PRIMARY KEY CLUSTERED 
(
	[IdCategoria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TbClientes]    Script Date: 2/20/2020 10:05:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TbClientes](
	[IdCliente] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Apellido] [varchar](50) NOT NULL,
	[Cedula] [varchar](13) NOT NULL,
	[Direccion] [varchar](100) NULL,
	[Telefono] [varchar](12) NULL,
	[Email] [varchar](100) NULL,
	[EliminadoPor] [varchar](50) NULL,
	[EliminadoEn] [datetime] NULL,
 CONSTRAINT [PK_TbClientes] PRIMARY KEY CLUSTERED 
(
	[IdCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TbCompras]    Script Date: 2/20/2020 10:05:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TbCompras](
	[IdCompras] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[PrecioCompra] [decimal](18, 2) NOT NULL,
	[Cantidad] [int] NOT NULL,
	[IdProveedor] [int] NOT NULL,
	[IdCategoria] [int] NOT NULL,
	[IdArticulo] [int] NOT NULL,
	[Codigo] [varchar](10) NOT NULL,
	[CostoUnitario] [decimal](18, 2) NOT NULL,
	[EliminadoPor] [varchar](50) NULL,
	[EliminadoEn] [datetime] NULL,
 CONSTRAINT [PK_TbCompras] PRIMARY KEY CLUSTERED 
(
	[IdCompras] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TbCuentasPorCobrar]    Script Date: 2/20/2020 10:05:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TbCuentasPorCobrar](
	[IdCuenta] [int] IDENTITY(1,1) NOT NULL,
	[IdVenta] [int] NOT NULL,
	[IdCliente] [int] NULL,
	[Fecha] [datetime] NOT NULL,
	[MontoTotal] [decimal](18, 2) NOT NULL,
	[MontoPendiente] [decimal](18, 2) NOT NULL,
	[MontoAbonado] [decimal](18, 2) NOT NULL,
	[EliminadoPor] [varchar](50) NULL,
	[EliminadoEn] [datetime] NULL,
 CONSTRAINT [PK_TbCuentasPorCobrar] PRIMARY KEY CLUSTERED 
(
	[IdCuenta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TbDevolucionesCompras]    Script Date: 2/20/2020 10:05:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TbDevolucionesCompras](
	[IdDevolucion] [int] IDENTITY(1,1) NOT NULL,
	[RazonDevolucion] [varchar](100) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[IdCompras] [int] NOT NULL,
	[IdArticulo] [int] NOT NULL,
	[Codigo] [varchar](10) NOT NULL,
	[IdProveedor] [int] NOT NULL,
	[MontoTotal] [decimal](18, 2) NOT NULL,
	[EliminadoPor] [varchar](50) NULL,
	[EliminadoEn] [datetime] NULL,
 CONSTRAINT [PK_TbDevolucionesCompras] PRIMARY KEY CLUSTERED 
(
	[IdDevolucion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TbDevolucionesVentas]    Script Date: 2/20/2020 10:05:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TbDevolucionesVentas](
	[IdDevolucion] [int] IDENTITY(1,1) NOT NULL,
	[RazonDevolucion] [varchar](100) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[IdVenta] [int] NOT NULL,
	[IdArticulo] [int] NOT NULL,
	[Codigo] [varchar](10) NOT NULL,
	[IdCliente] [int] NULL,
	[MontoTotal] [decimal](18, 2) NOT NULL,
	[EliminadoPor] [varchar](50) NULL,
	[EliminadoEn] [datetime] NULL,
 CONSTRAINT [PK_TbDevolucionesVentas] PRIMARY KEY CLUSTERED 
(
	[IdDevolucion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TbGastos]    Script Date: 2/20/2020 10:05:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TbGastos](
	[IdGasto] [int] IDENTITY(1,1) NOT NULL,
	[Lugar] [varchar](100) NULL,
	[TotalGastado] [decimal](18, 2) NULL,
	[EliminadoPor] [varchar](50) NULL,
	[EliminadoEn] [datetime] NULL,
 CONSTRAINT [PK_TbGastos] PRIMARY KEY CLUSTERED 
(
	[IdGasto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TbInventario]    Script Date: 2/20/2020 10:05:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TbInventario](
	[IdInventario] [int] IDENTITY(1,1) NOT NULL,
	[IdCategoria] [int] NOT NULL,
	[IdArticulo] [int] NOT NULL,
	[Codigo] [varchar](10) NOT NULL,
	[TotalArticulo] [int] NULL,
	[TotalVendido] [int] NULL,
	[TotalRestante] [int] NULL,
 CONSTRAINT [PK_TbInventario] PRIMARY KEY CLUSTERED 
(
	[IdInventario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TbProveedores]    Script Date: 2/20/2020 10:05:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TbProveedores](
	[IdProveedor] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[RazonSocial] [varchar](30) NOT NULL,
	[Cedula] [varchar](13) NULL,
	[Direccion] [varchar](100) NULL,
	[Telefono] [varchar](12) NULL,
	[Email] [varchar](100) NULL,
	[ConRNC] [bit] NULL,
	[RNC] [varchar](15) NULL,
	[Estado] [bit] NULL,
	[EliminadoPor] [varchar](50) NULL,
	[EliminadoEn] [datetime] NULL,
 CONSTRAINT [PK_TbProveedores] PRIMARY KEY CLUSTERED 
(
	[IdProveedor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TbUsuarios]    Script Date: 2/20/2020 10:05:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TbUsuarios](
	[IdUsuario] [int] IDENTITY(1,1) NOT NULL,
	[Usuario] [varchar](50) NOT NULL,
	[Contraseña] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TbUsuarios] PRIMARY KEY CLUSTERED 
(
	[IdUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TbVentas]    Script Date: 2/20/2020 10:05:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TbVentas](
	[IdVenta] [int] IDENTITY(1,1) NOT NULL,
	[IdCliente] [int] NULL,
	[ConDescuento] [bit] NOT NULL,
	[Descuento] [decimal](18, 2) NULL,
	[PrecioTotalVenta] [decimal](18, 2) NOT NULL,
	[PrecioConDescuento] [decimal](18, 2) NULL,
	[MetodoDePago] [varchar](40) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[EliminadoPor] [varchar](50) NULL,
	[EliminadoEn] [datetime] NULL,
 CONSTRAINT [PK_TbVentas] PRIMARY KEY CLUSTERED 
(
	[IdVenta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ArticulosVendidos]  WITH CHECK ADD  CONSTRAINT [FK__ArticulosVendido__2E1BDC42] FOREIGN KEY([IdArticulo], [Codigo])
REFERENCES [dbo].[TbArticulos] ([IdArticulo], [Codigo])
GO
ALTER TABLE [dbo].[ArticulosVendidos] CHECK CONSTRAINT [FK__ArticulosVendido__2E1BDC42]
GO
ALTER TABLE [dbo].[TbArticulos]  WITH CHECK ADD  CONSTRAINT [FK__TbArticul__IdCat__276EDEB3] FOREIGN KEY([IdCategoria])
REFERENCES [dbo].[TbCategoria] ([IdCategoria])
GO
ALTER TABLE [dbo].[TbArticulos] CHECK CONSTRAINT [FK__TbArticul__IdCat__276EDEB3]
GO
ALTER TABLE [dbo].[TbCompras]  WITH CHECK ADD  CONSTRAINT [FK__TbCompras__31EC6D26] FOREIGN KEY([IdArticulo], [Codigo])
REFERENCES [dbo].[TbArticulos] ([IdArticulo], [Codigo])
GO
ALTER TABLE [dbo].[TbCompras] CHECK CONSTRAINT [FK__TbCompras__31EC6D26]
GO
ALTER TABLE [dbo].[TbCompras]  WITH CHECK ADD  CONSTRAINT [FK__TbCompras__Elimi__30F848ED] FOREIGN KEY([IdCategoria])
REFERENCES [dbo].[TbCategoria] ([IdCategoria])
GO
ALTER TABLE [dbo].[TbCompras] CHECK CONSTRAINT [FK__TbCompras__Elimi__30F848ED]
GO
ALTER TABLE [dbo].[TbCompras]  WITH CHECK ADD  CONSTRAINT [FK__TbCompras__IdPro__32E0915F] FOREIGN KEY([IdProveedor])
REFERENCES [dbo].[TbProveedores] ([IdProveedor])
GO
ALTER TABLE [dbo].[TbCompras] CHECK CONSTRAINT [FK__TbCompras__IdPro__32E0915F]
GO
ALTER TABLE [dbo].[TbCuentasPorCobrar]  WITH CHECK ADD  CONSTRAINT [FK__TbCuentas__Elimi__45F365D3] FOREIGN KEY([IdVenta])
REFERENCES [dbo].[TbVentas] ([IdVenta])
GO
ALTER TABLE [dbo].[TbCuentasPorCobrar] CHECK CONSTRAINT [FK__TbCuentas__Elimi__45F365D3]
GO
ALTER TABLE [dbo].[TbCuentasPorCobrar]  WITH CHECK ADD  CONSTRAINT [FK__TbCuentas__IdCli__46E78A0C] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[TbClientes] ([IdCliente])
GO
ALTER TABLE [dbo].[TbCuentasPorCobrar] CHECK CONSTRAINT [FK__TbCuentas__IdCli__46E78A0C]
GO
ALTER TABLE [dbo].[TbDevolucionesCompras]  WITH CHECK ADD  CONSTRAINT [FK__TbDevoluc__Elimi__35BCFE0A] FOREIGN KEY([IdCompras])
REFERENCES [dbo].[TbCompras] ([IdCompras])
GO
ALTER TABLE [dbo].[TbDevolucionesCompras] CHECK CONSTRAINT [FK__TbDevoluc__Elimi__35BCFE0A]
GO
ALTER TABLE [dbo].[TbDevolucionesCompras]  WITH CHECK ADD  CONSTRAINT [FK__TbDevoluc__IdPro__37A5467C] FOREIGN KEY([IdProveedor])
REFERENCES [dbo].[TbProveedores] ([IdProveedor])
GO
ALTER TABLE [dbo].[TbDevolucionesCompras] CHECK CONSTRAINT [FK__TbDevoluc__IdPro__37A5467C]
GO
ALTER TABLE [dbo].[TbDevolucionesCompras]  WITH CHECK ADD  CONSTRAINT [FK__TbDevolucionesCo__36B12243] FOREIGN KEY([IdArticulo], [Codigo])
REFERENCES [dbo].[TbArticulos] ([IdArticulo], [Codigo])
GO
ALTER TABLE [dbo].[TbDevolucionesCompras] CHECK CONSTRAINT [FK__TbDevolucionesCo__36B12243]
GO
ALTER TABLE [dbo].[TbDevolucionesVentas]  WITH CHECK ADD  CONSTRAINT [FK__TbDevoluc__Elimi__412EB0B6] FOREIGN KEY([IdVenta])
REFERENCES [dbo].[TbVentas] ([IdVenta])
GO
ALTER TABLE [dbo].[TbDevolucionesVentas] CHECK CONSTRAINT [FK__TbDevoluc__Elimi__412EB0B6]
GO
ALTER TABLE [dbo].[TbDevolucionesVentas]  WITH CHECK ADD  CONSTRAINT [FK__TbDevoluc__IdCli__4316F928] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[TbClientes] ([IdCliente])
GO
ALTER TABLE [dbo].[TbDevolucionesVentas] CHECK CONSTRAINT [FK__TbDevoluc__IdCli__4316F928]
GO
ALTER TABLE [dbo].[TbDevolucionesVentas]  WITH CHECK ADD  CONSTRAINT [FK__TbDevolucionesVe__4222D4EF] FOREIGN KEY([IdArticulo], [Codigo])
REFERENCES [dbo].[TbArticulos] ([IdArticulo], [Codigo])
GO
ALTER TABLE [dbo].[TbDevolucionesVentas] CHECK CONSTRAINT [FK__TbDevolucionesVe__4222D4EF]
GO
ALTER TABLE [dbo].[TbInventario]  WITH CHECK ADD  CONSTRAINT [FK__TbInventa__Total__3A81B327] FOREIGN KEY([IdCategoria])
REFERENCES [dbo].[TbCategoria] ([IdCategoria])
GO
ALTER TABLE [dbo].[TbInventario] CHECK CONSTRAINT [FK__TbInventa__Total__3A81B327]
GO
ALTER TABLE [dbo].[TbInventario]  WITH CHECK ADD  CONSTRAINT [FK__TbInventario__3B75D760] FOREIGN KEY([IdArticulo], [Codigo])
REFERENCES [dbo].[TbArticulos] ([IdArticulo], [Codigo])
GO
ALTER TABLE [dbo].[TbInventario] CHECK CONSTRAINT [FK__TbInventario__3B75D760]
GO
ALTER TABLE [dbo].[TbVentas]  WITH CHECK ADD  CONSTRAINT [FK__TbVentas__Elimin__3E52440B] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[TbClientes] ([IdCliente])
GO
ALTER TABLE [dbo].[TbVentas] CHECK CONSTRAINT [FK__TbVentas__Elimin__3E52440B]
GO
/****** Object:  StoredProcedure [dbo].[ObtenerArticulosFacturados]    Script Date: 2/20/2020 10:05:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ObtenerArticulosFacturados]
@IdVenta int

as 

SELECT a.IdVenta, a.ConDescuento, a.Descuento, a.MetodoDePago, a.Fecha,
b.IdArticuloVendido, b.Codigo, c.Descripcion, d.TotalArticulo,
b.PrecioUnitario, b.MontoTotal, c.PrecioAlPorMayor, a.PrecioConDescuento 
FROM TbVentas a 
INNER JOIN ArticulosVendidos b ON a.IdVenta = b.IdVenta 
INNER JOIN TbArticulos c ON c.IdArticulo = b.IdArticulo
INNER JOIN TbInventario d on d.IdArticulo = c.IdArticulo

where a.IdVenta = @IdVenta
GO
